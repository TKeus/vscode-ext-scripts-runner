import path = require('path');
import * as vscode from 'vscode';
import * as os from 'os';
import * as fs from 'fs';

/****************************************************************************
 *
 *      Extension Model
 *
 ****************************************************************************/

interface IConfigBaseNode {
  type: string;
  name: string;
  icon?: string;
  hide?: boolean;
  expanded?: boolean;
  children?: ConfigNode[];
  description?: string;
}

interface IFolderConfig extends IConfigBaseNode {
  type: "folder";
  workingPath: string;
  preferredSubFolder?: string;
  packageSubFolder?: string;
}

interface IScriptConfig extends IConfigBaseNode {
  type: "script";
  terminal?: TerminalScript;
  script: string[];
}

type ConfigNode = IFolderConfig | IScriptConfig;

type TerminalScript = 'batch' | 'powershell' | '';

interface IExtensionConfig {
  tree: ConfigNode[];
}

/****************************************************************************
 *
 *       Utility functions
 *
 ****************************************************************************/

// a function returning path to assets icon files
function iconFilePath(iconFileName: string): string {
  return path.join(__filename, '..', '..', 'assets/icons', `${Configuration.darkMode ? 'dark' : 'light'}`, iconFileName);
}

function standardizeFilePath(filePath: string): string {
  let result = filePath.replace(/^~/, os.homedir());
  return result;
}

function getContentsOfFile(filePath: string): string {
  let result: string = '';
  filePath = standardizeFilePath(filePath);
   if (fs.existsSync(filePath)) {
    result = fs.readFileSync(filePath, 'utf-8').trim();
  }
  return result;
}

function waitForFileChange(filePath: string, callback: () => void) {
  const startTime = Date.now();
  const timeout = 300000;
  let fileChanged = false;
  Debug.log(`waitForFileChange: ${filePath}`);
  const interval = setInterval(() => {
    Debug.log(`Check file: ${filePath}`);
    if (!fileChanged && fs.existsSync(filePath) && fs.statSync(filePath).mtimeMs > startTime) {
      Debug.log(`-> file changed: ${filePath}`);
      fileChanged = true;
    }
    if (fileChanged || Date.now() - startTime >= timeout) {
      clearInterval(interval);
      Debug.log(`-> file changed or timeout: ${filePath}`);
      callback();
    }
  }, 3000);
}

/****************************************************************************
 *
 *       Configuration (vscode settings + extension setup file)
 *
 ****************************************************************************/

class Configuration {

  static debugEnabled: boolean = false;
  static scriptsRunnerConfigPath: string = '';
  static darkMode: boolean = false;
  static extensionConfig: IExtensionConfig;

  static load(): void {
    Configuration.debugEnabled = !!vscode.workspace.getConfiguration('scripts-runner.debug').get('enabled');

    var configPath = vscode.workspace.getConfiguration('scripts-runner.configuration').get('path', '~/development');
    configPath = standardizeFilePath(configPath);
    Configuration.scriptsRunnerConfigPath = configPath;
    Configuration.darkMode = vscode.workspace.getConfiguration('scripts-runner.configuration').get('dark', false);

    configPath = path.join(configPath, 'scripts-runner.json');
    Debug.log(`Get config from: ${configPath}`);
    if (fs.existsSync(configPath)) {
      this.extensionConfig = JSON.parse(fs.readFileSync(configPath, 'utf-8'));
    }
    Debug.log(`dark mode is ${Configuration.darkMode ? 'enabled' : 'disabled'}`);
  }
}

/****************************************************************************
 *
 *       Debug   (OUTPUT > Scripts-Runner)
 *
 ****************************************************************************/

class Debug {
  private static _debugOutput: vscode.OutputChannel = vscode.window.createOutputChannel("Scripts-Runner");

  static log(dbgString: string, show: boolean = false): void {
    if (Configuration.debugEnabled) {
      this._debugOutput.appendLine(dbgString);
      if (show) {
        this._debugOutput.show();
      }
    }
  }
}

/****************************************************************************
 *
 *       Script runner
 *
 ****************************************************************************/

class ScriptRunner {
  private static _terminal: vscode.Terminal | undefined;

  static ensureTerminalExists(terminal?: TerminalScript): vscode.Terminal {
    const terminalName = (terminal === 'powershell') ? 'Scripts Runner - PowerShell' : 'Scripts Runner';

    // Search for an existing terminal with the same name
    ScriptRunner._terminal = vscode.window.terminals.find(t => t.name === terminalName);
    // If no existing terminal found, create a new one
    if (!ScriptRunner._terminal) {
      const shellPath = terminal === 'powershell' ? 'PowerShell.exe' : undefined;
      ScriptRunner._terminal = vscode.window.createTerminal({ name: terminalName, shellPath });
    }
    ScriptRunner._terminal.show(false);

    // if user closes the terminal, delete our reference:
    vscode.window.onDidCloseTerminal((closedTerminal) => {
      if (closedTerminal.name === terminalName) {
        ScriptRunner._terminal = undefined;
      }
    });
    return ScriptRunner._terminal;
  };

  static executeScript(scriptTerminal: TerminalScript | undefined, scriptLines: string[]) {
    const terminal = ScriptRunner.ensureTerminalExists(scriptTerminal);
    if (terminal) {
      scriptLines.forEach((scriptLine) => {
        terminal.sendText(scriptLine, true);
      });
    }
  }
}

/****************************************************************************
 *
 *       Base Node
 *
 ****************************************************************************/

class BaseNode extends vscode.TreeItem {
  public readonly config: ConfigNode;
  public readonly parent: BaseNode | undefined;

  constructor(name: string, config: ConfigNode, parent: BaseNode | undefined) {
    super(name);
    this.config = config;
    this.parent = parent;
    this.contextValue = config.type;

    const visibleChildrenCount = (config.children || []).reduce((count, child) => count + (!child.hide ? 1 : 0), 0);
    this.collapsibleState = (visibleChildrenCount > 0) ? (this.config.expanded ? vscode.TreeItemCollapsibleState.Expanded : vscode.TreeItemCollapsibleState.Collapsed) : vscode.TreeItemCollapsibleState.None;
    this.updateLabel();
  }

  getBestDescription(): string | undefined {
    if (this.config.description && this.config.description.trim() !== '') {
      return this.config.description.trim();
    }
    if (this.parent) {
      return this.parent.getBestDescription();
    }
    return undefined;
  }

  // Get assynchronous children
  static getChildren(subElements: ConfigNode[] | undefined, parent: BaseNode | undefined): Promise<BaseNode[]> { // get Children for provider
    const results: BaseNode[] = [];
    if (subElements) {
      return new Promise(resolve => {
        subElements.forEach((elementConfig) => {
          if (elementConfig.type === "folder") {
            results.push(new FolderNode(elementConfig, parent));
          } else {
            results.push(new ScriptNode(elementConfig, parent));
          };
        });
        resolve(results);
      });
    } else {
      return Promise.resolve([]);
    }
  }

  getMoreLabelInfo(): string {
    return '';
  }

  updateLabel(): void {
    this.label = this.config.name;
    const sup: string = this.getMoreLabelInfo();
    if (sup !== '') {
      this.label += ' ' + sup;
    }
    if (this.config.description) {
      this.label += ' - ' + getContentsOfFile(this.config.description);
    }
    if (this.parent) {
      this.parent.updateLabel();
    }
    Debug.log(`Update label: ${this.label}`);
  }
}

/****************************************************************************
 *
 *       Script Node
 *
 ****************************************************************************/

class ScriptNode extends BaseNode {
  public readonly config: IScriptConfig;

  constructor(config: IScriptConfig, parent: BaseNode | undefined) {
    super(config.name, config, parent);
    this.config = config;

    this.tooltip = `${this.label} -> ${this.config.name}`;
    Debug.log(`Add Script: ${this.tooltip}`);
    this.iconPath = iconFilePath(this.config.icon ? this.config.icon : 'hello.svg');
  }

  iconPath = iconFilePath('run.svg');

  execute() {
    Debug.log(`Execute Script "${this.label}"`);
    const descFile = this.getBestDescription();
    if (descFile) {
      Debug.log(`waitForFileChange ? : ${descFile}`);
      waitForFileChange(descFile, () => { vscode.commands.executeCommand('scripts-runner.refreshEntry'); });
    }
    ScriptRunner.executeScript(this.config.terminal, this.config.script);
  }
}

/****************************************************************************
 *
 *       Folder Node
 *
 ****************************************************************************/

class FolderNode extends BaseNode {
  public readonly config: IFolderConfig;
  public readonly packageFilePath: string;

  updateLabel(): void {
    super.updateLabel();
    this.tooltip = `${this.label}`;
  }

  getMoreLabelInfo(): string {
    let version = '';
    if (fs.existsSync(this.packageFilePath)) {
      const packageJson = JSON.parse(fs.readFileSync(this.packageFilePath, 'utf-8'));
      version = `   (${packageJson.version})`;
    }
    return version;
  }

  constructor(config: IFolderConfig, parent: BaseNode | undefined) {
    super(config.name, config, parent);
    this.config = config;

    this.packageFilePath = path.join(config.workingPath,
      config.packageSubFolder || config.preferredSubFolder || '', 'package.json');
    Debug.log(`Add project: ${this.tooltip}(${config.workingPath})`);

    this.iconPath = iconFilePath(this.config.icon || 'folder-open.svg');
  }
}

/****************************************************************************
 *
 *       Projects Provider
 *
 ****************************************************************************/

class TreeNodesProvider implements vscode.TreeDataProvider<(BaseNode)> {

  getTreeItem(element: BaseNode): vscode.TreeItem {
    return element;
  }

  getChildren(element?: BaseNode): Promise<BaseNode[]> {
    let results: Promise<BaseNode[]> = Promise.resolve([]);
    if (!Configuration.extensionConfig) {
      vscode.window.showInformationMessage('Configuration could not be found');
      Debug.log('Configuration could not be found');
      return results;
    }
    // get children of the element, or the root children (if no element is specified)
    const subElements: (ConfigNode[] | undefined) = element ? element.config.children : Configuration.extensionConfig.tree;
    if (subElements) {
      results = BaseNode.getChildren(subElements, element);
    }
    return results;
  }

  private _onDidChangeTreeData: vscode.EventEmitter<BaseNode | undefined | null | void> = new vscode.EventEmitter<BaseNode | undefined | null | void>();
  readonly onDidChangeTreeData: vscode.Event<BaseNode | undefined | null | void> = this._onDidChangeTreeData.event;

  refresh(): void {
    Debug.log('Refresh !');
    Configuration.load();
    this._onDidChangeTreeData.fire();
  }

  runScript(script: ScriptNode): void {
    this._onDidChangeTreeData.fire(script);
    script.execute();
  }
}

/****************************************************************************
 * 
 * ScriptsRunnerExtension
 * 
 ****************************************************************************/

class ScriptsRunnerExtension {
  static context: vscode.ExtensionContext;
  static treeNodesProvider: TreeNodesProvider;

  static load(context: vscode.ExtensionContext) {
    this.context = context;

    Configuration.load(); // First load configuration

    this.treeNodesProvider = new TreeNodesProvider();
    context.subscriptions.push(
      vscode.window.registerTreeDataProvider('scripts-runner.view', this.treeNodesProvider));
    context.subscriptions.push(
      vscode.window.createTreeView('scripts-runner.view', { treeDataProvider: this.treeNodesProvider }));
    context.subscriptions.push(
      vscode.commands.registerCommand('scripts-runner.refreshEntry', () => { this.treeNodesProvider.refresh(); }));
    context.subscriptions.push(
      vscode.commands.registerCommand('scripts-runner.runScript', (resource) => { this.treeNodesProvider.runScript(resource); }));
  }
}

// The extension is activated
export function activate(context: vscode.ExtensionContext) {
  ScriptsRunnerExtension.load(context);
}

// the extension is deactivated
export function deactivate() { }
