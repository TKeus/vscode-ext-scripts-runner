# Scripts Runner Extension for VSCode

This extension helps build and perform development tasks. It allows to run scripts sorted by project.  
The scripts will use a VSCode internal terminal (Cmd or PowerShell).

<p align="center">
  <a href="#features">Features</a> •
  <a href="#how-to-build">How To Build</a> •
  <a href="#credits">Credits</a> •
  <a href="#license">License</a>
</p>

![screenshot](./assets/screenshots/extension-in-vscode.png)

## Features

* Show a list of scripts, sorted in a tree.
  * Organize scripts in folders, and sub-folders
  * Hide some nodes
  * Display expanded/collapsed
* Execute scripts in internal terminal (cmd or PoweShell)

## How To Build

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/TKeus/vscode-ext-scripts-runner

# Go into the repository
$ cd vscode-ext-scripts-runner

# Install dependencies
$ npm install

# Build the extension
$ npm run build
```

- Start VSCode, and goto the 'Extensions' panel.  
- From the EXTENSIONS menu, select 'Install from VSIX...' and select the vsix file that was built.

The extension is now installed in VSCode. You can setup your scripts.

## Credits

This software uses open-licensed SVG vectors found on [SVG Repo](https://www.svgrepo.com/).

## License

[MIT](./LICENSE)

---

> [Thierry Keus](https://tkeus.gitlab.io/my-resume/) &nbsp;&middot;&nbsp; GitLab [@TKeus](https://gitlab.com/TKeus)

