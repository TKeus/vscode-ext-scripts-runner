# Change Log

All notable changes to the "vscode-scripts-runner" project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.0.0]

### Added

* Show a list of scripts, sorted in a tree.
  * Organize scripts in folders, and sub-folders
  * Hide some nodes
  * Display expanded/collapsed
* Execute scripts in internal terminal (cmd or PoweShell)
  